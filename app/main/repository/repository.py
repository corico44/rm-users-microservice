from app.main.config import db

# Get collection "users"
user_collection = db["users"]


def user_with_email_exists(user):
    if user_collection.find({"email": user.email}).count() > 0:
        return True
    else:
        return False


def user_with_email_and_password_exists(user):
    if user_collection.find({"email": user.email, "password": user.password}).count() > 0:
        return True
    else:
        return False


def create_user(user):
    mydict = {"name": user.name, "email": user.email, "password": user.password, "points": 0}
    inserted_message = user_collection.insert_one(mydict)
    return inserted_message


def get_user_with_email(email):
    return user_collection.find({"email": email})


def add_points_user(user):
    currentPoints = get_user_with_email(user.email)
    for doc in currentPoints:
        finalPoints = doc["points"] + user.points
        myquery = {"email": user.email}
        newvalues = {"$set": {"points": finalPoints}}
        inserted_message = user_collection.update_one(myquery, newvalues)
        return inserted_message


def user_has_visited_buildings(user_email):
    return user_collection.find({"email": user_email, "visited": {"$exists": True}})


def add_visited_building(user_email, building_name):
    if user_collection.find({"email": user_email, "visited": building_name}).count() == 0:
        myquery = {"email": user_email}
        newvalues = {"$push": {"visited": building_name}}
        inserted_message = user_collection.update_one(myquery, newvalues)
        return inserted_message


def get_ranking():
    return user_collection.find({}, {"name": 1, "points": 1}).sort("points", -1).limit(10)
