from beanie import PydanticObjectId
from pydantic import BaseModel, Field
from typing import Optional


class UserLogin(BaseModel):
    id: Optional[PydanticObjectId] = Field(None, alias="_id")
    email: str
    password: str
