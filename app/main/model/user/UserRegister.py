from beanie import PydanticObjectId
from pydantic import BaseModel, Field
from typing import Optional


class UserRegister(BaseModel):
    id: Optional[PydanticObjectId] = Field(None, alias="_id")
    name: str
    email: str
    password: str
    points: int
