from beanie import PydanticObjectId
from pydantic import BaseModel, Field
from typing import Optional


class UserPoints(BaseModel):
    id: Optional[PydanticObjectId] = Field(None, alias="_id")
    email: str
    points: int
