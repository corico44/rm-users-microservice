from app.main.repository import repository


def create_new_user(user):
    user_found = repository.user_with_email_exists(user)
    if not user_found:
        inserted_message = repository.create_user(user)

        if inserted_message:
            return {
                       "code": 201,
                       "message": "User successfully created"
                   }, 201
        else:
            return {
                       "code": 400,
                       "message": "Bad request"
                   }, 400

    else:
        return {
                   "code": 409,
                   "message": "User already in database"
               }, 409


def login_user(user):
    user_found = repository.user_with_email_and_password_exists(user)
    if user_found:
        return {
                   "code": 200,
                   "message": "User successfully logged in"
               }, 200
    else:
        return {
                   "code": 401,
                   "message": "User can't be found"
               }, 401


def get_user(email):
    user_found = repository.get_user_with_email(email)
    for doc in user_found:
        if user_has_visited_buildings(email).count() != 0:
            return {
                       "name": doc["name"],
                       "email": doc["email"],
                       "password": doc["password"],
                       "points": doc["points"],
                       "visited": doc["visited"]
                   }, 200
        else:
            return {
                       "name": doc["name"],
                       "email": doc["email"],
                       "password": doc["password"],
                       "points": doc["points"]
                   }, 200


def get_ranking():
    users_found = repository.get_ranking()
    result = []
    for doc in users_found:
        result.append({
           "name": doc["name"],
           "points": doc["points"],
           })

    return result, 200


def add_points_user(user):
    inserted_points = repository.add_points_user(user)
    if inserted_points:
        return {
                   "code": 200,
                   "message": "Points succesfully added"
               }, 200
    else:
        return {
                   "code": 401,
                   "message": "Points cant be added"
               }, 401


def user_has_visited_buildings(user_email):
    return repository.user_has_visited_buildings(user_email)


def add_visited_building(user_email, building_name):
    inserted_visited_building = repository.add_visited_building(user_email, building_name)
    if inserted_visited_building:
        return {
                   "code": 200,
                   "message": "Visited building succesfully added"
               }, 200
    else:
        return {
                   "code": 401,
                   "message": "Visited building cant be added"
               }, 401
