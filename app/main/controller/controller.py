from flask_restx import Resource, fields, Namespace
from flask import request

from ..model.user.UserRegister import UserRegister
from ..model.user.UserLogin import UserLogin
from ..model.user.UserPoints import UserPoints
from ..service.service import create_new_user, get_user, login_user, add_points_user, add_visited_building, \
    get_ranking

api = Namespace('user', description='user related operations')

userRegisterNamespace = Namespace("User Register")
userLoginNamespace = Namespace("User Login")
userPointsNamespace = Namespace("User Points")
userVisitedBuildingNamespace = Namespace("User Visited Building")


@api.route('/register')
class Register(Resource):
    @api.response(201, 'User successfully registered')
    @api.response(409, 'User already in database')
    @api.response(400, 'Wrong parameters')
    @api.doc('Register a new user')
    @api.expect(api.model('User Register', {
        'name': fields.String(required=True, description='user username'),
        'email': fields.String(required=True, description='user email address'),
        'password': fields.String(required=True, description='user password')
    }), validate=True)
    def post(self):
        """Register a new User """
        data = request.json
        user = UserRegister(name=data['name'], email=data['email'], password=data['password'], points=0)
        return create_new_user(user)


@api.route('/login')
class Login(Resource):
    @api.response(200, 'User successfully logged')
    @api.response(401, 'Email or password does not match')
    @api.doc('Login a user')
    @api.expect(api.model('User Login', {
        'email': fields.String(required=True, description='user email address'),
        'password': fields.String(required=True, description='user password')
    }), validate=True)
    def post(self):
        """Login a User """
        data = request.json
        user = UserLogin(email=data['email'], password=data['password'])
        return login_user(user)


@api.route('/<email>')
@api.param('email', 'The User email')
@api.response(404, 'User not found')
class GetUser(Resource):
    @api.doc('Get a user')
    def get(self, email):
        """get a user given its identifier"""
        user = get_user(email)
        if not user:
            api.abort(404)
        else:
            return user


@api.route('/addPoints')
class AddPoints(Resource):
    @api.response(200, 'Points successfully added')
    @api.response(401, 'Points cant be added')
    @api.doc('Add points to user')
    @api.expect(api.model('User Points', {
        'email': fields.String(required=True, description='user email address'),
        'points': fields.Integer(required=True, description='user points')
    }), validate=True)
    def post(self):
        """Add points to User"""
        data = request.json
        user = UserPoints(email=data['email'], points=data['points'])
        return add_points_user(user)


@api.route('/addVisitedBuilding')
class AddVisitedBuilding(Resource):
    @api.response(200, 'Visited building succesfully added')
    @api.response(401, 'Visited building cant be added')
    @api.doc('Add visited building to user')
    @api.expect(api.model('User Visited Building', {
        'email': fields.String(required=True, description='user email address'),
        'visitedBuildingName': fields.String(required=True, description='visited building name')
    }), validate=True)
    def post(self):
        """Add visited building to User"""
        data = request.json
        email = data['email']
        visited_building_name = data['visitedBuildingName']
        return add_visited_building(email, visited_building_name)


@api.route('/getRanking')
@api.response(404, 'Users not found')
class GetRanking(Resource):
    @api.doc('Get ranking')
    def get(self):
        """get ranking"""
        users = get_ranking()
        if not users:
            api.abort(404)
        else:
            return users
