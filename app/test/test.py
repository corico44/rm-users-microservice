import random
import string

from locust import HttpUser, task, between

list = []


class WebsiteTestUser(HttpUser):
    wait_time = between(0.5, 5.0)

    def on_start(self):
        pass

    def on_stop(self):
        pass

    @task(1)
    def get_all_users(self):
        self.client.get("http://reus-modernista-users.herokuapp.com/users/getRanking")

    @task(2)
    def get_user(self):
        self.client.get("http://reus-modernista-users.herokuapp.com/users/test2@gmail.com")

    @task(3)
    def login(self):
        self.client.post("http://reus-modernista-users.herokuapp.com/users/login",
                         json={"email": "test2@gmail.com", "password": "1234"})

    @task(4)
    def addVisitedBuilding(self):
        value = random.randint(0, 100000000)
        if value not in list:
            list.append(value)
            self.client.post("http://reus-modernista-users.herokuapp.com/users/addVisitedBuilding",
                             json={"email": "test2@gmail.com", "visitedBuildingName": "Casa " + str(value)})

    @task(5)
    def addPoints(self):
        points = random.randint(0, 100)
        self.client.post("http://reus-modernista-users.herokuapp.com/users/addPoints",
                         json={"email": "test2@gmail.com", "points": points})

    @task(6)
    def register(self):
        letters = string.ascii_lowercase
        mail = ''.join(random.choice(letters) for i in range(10))
        self.client.post("http://reus-modernista-users.herokuapp.com/users/register",
                         json={"name": "test", "email": mail + "@gmail.com", "password": "1234"})

