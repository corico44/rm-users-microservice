from flask import Blueprint
from flask_restx import Api
from .main.controller.controller import api as user_ns

blueprint = Blueprint('api', __name__)

api = Api(blueprint,
          title='REUS MODERNISTA',
          version='1.0',
          description='flask rest web service for reus modernista'
          )

api.add_namespace(user_ns, path='/users')
